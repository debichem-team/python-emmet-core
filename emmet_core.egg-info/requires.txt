numpy<2
pymatgen
monty>=2024.2.2
pydantic>=2.0
pydantic-settings>=2.0
pybtex~=0.24
typing-extensions>=3.7

[all]
matcalc>=0.0.4
seekpath>=2.0.1
robocrys>=0.2.8
pymatgen-analysis-defects>=2024.7.18
pymatgen-analysis-diffusion>=2024.7.15
pymatgen-analysis-alloys>=0.0.6
solvation-analysis>=0.4.1
MDAnalysis>=2.7.0

[docs]
mkdocs
mkdocs-material<8.3
mkdocs-material-extensions
mkdocs-minify-plugin
mkdocstrings
mkdocs-awesome-pages-plugin
mkdocs-markdownextradata-plugin
mkdocstrings[python]
livereload
jinja2

[ml]
chgnet
matgl
dgl<=2.1

[test]
pre-commit
pytest
pytest-cov
pycodestyle
pydocstyle
flake8
mypy
mypy-extensions
types-setuptools
types-requests
wincertstore
custodian
